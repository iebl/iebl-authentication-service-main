package com.iebl.vo.ext;



import org.springframework.data.jpa.domain.Specification;

public  class ExtendedSpecicationBean<T> {

	private Specification<T> specification;
	
	private OrderExtendedSpec orderExtendedExpec;
	
	private SelectionExtendedSpec selectionExtendedExpec; 
	
	public Specification<T> getSpecification() {
		return specification;
	}
	public void setSpecification(Specification<T> specification) {
		this.specification = specification;
	}
	public OrderExtendedSpec getOrderExtendedExpec() {
		return orderExtendedExpec;
	}
	public void setOrderExtendedExpec(OrderExtendedSpec orderExtendedExpec) {
		this.orderExtendedExpec = orderExtendedExpec;
	}
	public SelectionExtendedSpec getSelectionExtendedExpec() {
		return selectionExtendedExpec;
	}
	public void setSelectionExtendedExpec(SelectionExtendedSpec selectionExtendedExpec) {
		this.selectionExtendedExpec = selectionExtendedExpec;
	}
	
	

	
	
}
