package com.iebl.vo.ext;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

public abstract class OrderExtendedSpec {

	public abstract <T> Order[] getOrder(Root<T> root, CriteriaBuilder cb);


}
