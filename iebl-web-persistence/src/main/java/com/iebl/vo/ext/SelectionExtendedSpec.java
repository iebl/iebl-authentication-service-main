package com.iebl.vo.ext;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

public abstract class SelectionExtendedSpec {
	public  abstract <T>Selection<?>[] getSelection(Root<T> root, CriteriaBuilder cb);	
}
