package com.iebl.app.rep.ext;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;



import com.iebl.common.utils.ValidationUtil;
import com.iebl.vo.ext.ExtendedSpecicationBean;

public class ExtendedRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements ExtendedRepository<T, ID>{
	
	private EntityManager em;
	
	public ExtendedRepositoryImpl(Class<T> domainClass, EntityManager em0) {
        super(domainClass, em0);
        this.em=em0;
    }
	
	public ExtendedRepositoryImpl(JpaEntityInformation<T, ID> eI, EntityManager em0){
		super(eI,em0);
		this.em=em0;
	}
	@Override
	@Deprecated
	public List<T> findAllNoCount(Specification<T> aSpecification, Pageable aPageable) {
		    TypedQuery<T> query = getQuery(aSpecification, aPageable);
		    query.setFirstResult((int) aPageable.getOffset());
		    query.setMaxResults(aPageable.getPageSize());
		    return query.getResultList();
		}
	@Override
	public List<T> findAllNoCount(ExtendedSpecicationBean<T> eSpecification, Pageable aPageable){
		
		TypedQuery<T> query= getSelectQuery(eSpecification);
		query.setFirstResult((int) aPageable.getOffset());
	    query.setMaxResults(aPageable.getPageSize());
	    return query.getResultList();
		
	}
	@Override
	public Page<T> findAll(ExtendedSpecicationBean<T> extSpecification, Pageable aPageable){
		Page<T> page;
		TypedQuery<T> query= getSelectQuery(extSpecification);
		query.setFirstResult((int) aPageable.getOffset());
	    query.setMaxResults(aPageable.getPageSize());
	    page= new PageImpl<T>(query.getResultList(),aPageable,count(extSpecification));
	    return page;
	}
	
	@Deprecated
	@Override
	public List<T> findAllMaxResults(Specification<T> aSpecification,
			int maxResult) {
			TypedQuery<T> query = getQuery(aSpecification, (Sort)null);
		    return query.setMaxResults(maxResult).getResultList();
	}
	
	@Override
	public List<T> findAllMaxResults(ExtendedSpecicationBean<T> extSpecification,
			int maxResult) {
			TypedQuery<T> query = getSelectQuery(extSpecification);
		    return query.setMaxResults(maxResult).getResultList();
	}
	
	
	@Override
	public T selectFindOne(ExtendedSpecicationBean<T> eSpecification) {
			return getSelectQuerySingleResult(eSpecification);
		
	}
	@Override
	public List<T> selectFindAll(ExtendedSpecicationBean<T> eSpecification) {
		return getSelectQueryResultList(eSpecification);
	}
	
	@Override
	public List<T> selectFindAllMaxResults(ExtendedSpecicationBean<T> eSpecification,int maxResults) {
		return getSelectQueryMaxResultList(eSpecification,maxResults);
	}
	
	private T getSelectQuerySingleResult(ExtendedSpecicationBean<T> eSpecification){
		try{
			return getSelectQuery(eSpecification).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}
	private List<T> getSelectQueryResultList(ExtendedSpecicationBean<T> eSpecification){
		try{
			return getSelectQuery(eSpecification).getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	
	private List<T> getSelectQueryMaxResultList(ExtendedSpecicationBean<T> eSpecification,int maxresults){
		try{
			return getSelectQuery(eSpecification).setMaxResults(maxresults).getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	
/**
 * Clase que arma el query con el ExtendedSpecicationBean
 * @param eSpecification
 * @return
 */
	
	private TypedQuery<T> getSelectQuery(ExtendedSpecicationBean<T> eSpecification){
		Class<T> domainType = getDomainClass();
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<T> cq=cb.createQuery(domainType);
		Root<T> root=cq.from(domainType);
			//agrega un multiselect
		if(ValidationUtil.isNotNull(eSpecification.getSelectionExtendedExpec())){
				cq.multiselect(eSpecification.getSelectionExtendedExpec().getSelection(root, cb));
			
		}
		
		//agrega un OrderBy
		if(ValidationUtil.isNotNull(eSpecification.getOrderExtendedExpec())){
			cq.orderBy(eSpecification.getOrderExtendedExpec().getOrder(root, cb));
		}
		
		cq.where(eSpecification.getSpecification().toPredicate(root, cq, cb));
		
		
	    return em.createQuery(cq);
	    
	}
	

@SuppressWarnings("unchecked")
@Override
public long count(ExtendedSpecicationBean<T> extSpec) {
	Class<T> domainType = getDomainClass();
	CriteriaBuilder cb=em.getCriteriaBuilder();
	CriteriaQuery<T> cq=cb.createQuery(domainType);
	CriteriaQuery<Long> cql;
	Root<T> root=cq.from(domainType);
	
	cql=(CriteriaQuery<Long>) cq;
	cql.select(cb.count(root));

	//agrega un OrderBy
	if(ValidationUtil.isNotNull(extSpec.getOrderExtendedExpec())){
		cql.orderBy(extSpec.getOrderExtendedExpec().getOrder(root, cb));
	}
	
	cql.where(extSpec.getSpecification().toPredicate(root, cq, cb));	
    return em.createQuery(cql).getSingleResult();
}
	
	

}
