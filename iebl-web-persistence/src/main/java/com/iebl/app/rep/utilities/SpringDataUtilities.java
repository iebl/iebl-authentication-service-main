package com.iebl.app.rep.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.iebl.common.utils.ValidationUtil;




public class SpringDataUtilities {
	
	
	
	
	public static  final <TL,TR> Join<TL,TR> getJoin(Root<TL> rootL,String joinMetod,Class<TR> rootR){
		return rootL.join(joinMetod);
		
	}
	

	
	public static  final <TL,TR,TR2> Join<TR,TR2> getJoin(Root<TL> rootL,String joinMetod,Class<TR> rootR,String joinMetod2,Class<TR2> rootR2){
		return rootL.join(joinMetod).join(joinMetod2);
	}
	
	
	public static final Predicate addPredicateNotNull(CriteriaBuilder cb,Predicate pred,Object value){
		if(ValidationUtil.isNotNull(value)){
			return pred;
		}
		return cb.conjunction();
	}
	
	public static final Predicate addPredicateNotNull(CriteriaBuilder cb,Predicate pred,Collection<?> value){
		if(ValidationUtil.isNotNullOrEmpty(value)){
			return pred;
		}
		return cb.conjunction();
	}
	
	
	public static Predicate getPredicateEqual(CriteriaBuilder cb,Expression<?> exp,Object value){
		return addPredicateNotNull(cb,cb.equal(exp,value),value);
	}
	
	public static Predicate getPredicateNotEqual(CriteriaBuilder cb,Expression<?> exp,Object value){
		return addPredicateNotNull(cb,cb.notEqual(exp,value),value);
	}
	
	public static Predicate getPredicateIn(CriteriaBuilder cb,Expression<?> exp,Collection<?> value){
		if(ValidationUtil.isNotNullOrEmpty(value)){
			return exp.in(value);
		}else{
			return cb.conjunction();
		}
	}
	/**
	 * Agrega or cada valor de la lista para la expresion exp
	 * @param cb
	 * @param exp
	 * @param col
	 * @return
	 */
	public static Predicate getAndList(CriteriaBuilder cb,Expression<?> exp,Collection<?> col){
		List<Predicate> pred=new ArrayList<Predicate>();
		if(ValidationUtil.isNotNullOrEmpty(col)){
			
			for(Object value:col){
		
					pred.add(cb.equal(exp,value));
			}
			
		}else{
			return cb.conjunction();
			
		}
		Predicate[] arrayPred=new Predicate[pred.size()];
		return cb.and(pred.toArray(arrayPred));
	}	
	
	/**
	 * Between two dates
	 * @param cb
	 * @param exp
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Predicate betweenDate(CriteriaBuilder cb, Expression<Date> exp,
			Date startDate, Date endDate) {
		if(ValidationUtil.isNotNull(startDate)&&ValidationUtil.isNotNull(endDate)){
			return cb.between(exp, startDate, endDate);
		}else{
			return cb.conjunction();
		}
	}

	public static Predicate getPredicateUperLike(CriteriaBuilder cb,Expression<String> exp, String like) {
		if(ValidationUtil.isNotNull(like)){
			return cb.like(cb.upper(exp),"%"+like.toUpperCase()+"%");
		}
		return cb.conjunction();
		
	}
	
	public static Predicate getPredicateUperNotLike(CriteriaBuilder cb,Expression<String> exp, String like) {
	
		if(ValidationUtil.isNotNull(like)){
			return cb.not(cb.like(cb.upper(exp),"%"+like.toUpperCase()+"%"));
		}
		return cb.conjunction();
		
	}
	
	public static Predicate getPredicateNotLike(CriteriaBuilder cb,Expression<String> exp, String like) {
	
		if(ValidationUtil.isNotNull(like)){
			return cb.not(cb.like(exp,"%"+like+"%"));
		}
		return cb.conjunction();
	}
	
	public static Predicate getPredicatelike(CriteriaBuilder cb,Expression<String> exp, String like) {
		if(ValidationUtil.isNotNull(like)){
			return cb.like(exp,"%"+like+"%");
		}
		return cb.conjunction();
	}
	
	public static  <T> Specification<T> getSpecificationIfNotNull(Specification<T> spec,Collection<?> value){
		if(ValidationUtil.isNotNullOrEmpty(value)){
			return spec;
		}else{
			return (root, cq, cb)->cb.conjunction();
		}
		
	}
	public static  <T> Specification<T> getSpecificationIfNotNull(Specification<T> spec,Object value){
		if(ValidationUtil.isNotNull(value)){
			return spec;
		}else{
			return (root, cq, cb)->cb.conjunction();
		}
		
	}
	public static  <T> Specification<T> getSpecificationIfNotNull(Specification<T> spec,boolean value){
		if(value){
			return spec;
		}else{
			return (root, cq, cb)->cb.conjunction();
		}
		
	}
	
}
