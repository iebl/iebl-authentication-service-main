package com.iebl.app.rep;



import com.iebl.app.model.UserDO;
import com.iebl.app.rep.ext.ExtendedRepository;

public interface UserRepository extends ExtendedRepository<UserDO, Long>{

	UserDO findByEmailAndPassword();
	


	
}
