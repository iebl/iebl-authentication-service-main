package com.iebl.app.rep.ext;

import java.io.Serializable;
import java.util.List;








import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.iebl.vo.ext.ExtendedSpecicationBean;


@NoRepositoryBean
public interface ExtendedRepository<T, ID extends Serializable> 
  extends JpaRepository<T, ID> ,JpaSpecificationExecutor<T>{
  /**
   * Busca todos los resultados de la paginacion, pero solo cuenta la cantidad en el indice 0
   * @param aSpecification
   * @param aPageable
   * @return
   */
	@Deprecated
    List<T> findAllNoCount(Specification<T> aSpecification, Pageable aPageable);
	/**
	 * Busca todos los resultados de la paginacion, pero solo cuenta la cantidad en el indice 0
	 * Opcionalmente los ordena por el arreglo de order en la expecificacion de existir y o el select de la expecificacion extendida de existir
	 * @param aSpecification
	 * @param aPageable
	 * @param order
	 * @return
	 */
	List<T> findAllNoCount(ExtendedSpecicationBean<T> eSpecification, Pageable aPageable);
	/**
	 * Busca Los resultados de la especificacion
	 * Y solo muestra tantos resultados como se especifique en max results.
	 * @param aSpecification
	 * @param maxResult
	 * @return
	 */
	@Deprecated
    List<T> findAllMaxResults(Specification<T> aSpecification,int maxResult);
    /**
     * Busca Los resultados de la especificacion, solo muestra tantos resultados como se especifique en max results
     * Opcionalmente los ordena por el arreglo de order en la expecificacion de existir y o el select de la expecificacion extendida de existir
     * @param extSpecification
     * @param maxResult
     * @return
     */
	List<T> findAllMaxResults(ExtendedSpecicationBean<T> extSpecification,
			int maxResult);
    /**
     * Busca el resultado unico de un select especificado por lo que requiere de extended especification para obtenerlos
     * @param aSpecification
     * @return
     */
	T selectFindOne(ExtendedSpecicationBean<T> aSpecification);
	/**
	 * Busca Los resultados de un select especificado por lo que requiere de extended especification para obtenerlos
	 * @param eSpecification
	 * @return
	 */
	List<T> selectFindAll(ExtendedSpecicationBean<T> eSpecification);
	/**
	 * Busca Los resultados de un select especificado por lo que requiere de extended especification para obtenerlos
	 * Y solo muestra tantos resultados como se especifique en max results.
	 * @param eSpecification
	 * @param maxRAesults
	 * @return
	 */
	List<T> selectFindAllMaxResults(ExtendedSpecicationBean<T> eSpecification,int maxRAesults);
	/**
	 * cuenta y Busca los resultados de la paginacion
	 * Opcionalmente los ordena por el arreglo de order en la expecificacion de existir de la expecificacion extendida de existir
	 * @param aSpecification
	 * @param aPageable
	 * @return
	 */
	Page<T> findAll(ExtendedSpecicationBean<T> aSpecification,
			Pageable aPageable);
	/**
	 * cuenta el total de resultados de la especificacion Extendida
	 * @param extSpec
	 * @return
	 */
	long count(ExtendedSpecicationBean<T> extSpec);

}