package com.iebl.app.config;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.iebl.app.rep.ext.ExtendedRepositoryImpl;
/*
@Configuration
@EnableAsync
@EnableScheduling
@ImportResource("classpath:spring/persistence-context.xml")
@EnableJpaRepositories(
		basePackages = "com.iebl.app.rep",
		entityManagerFactoryRef="crmEntityManagerFactory",
		repositoryBaseClass = ExtendedRepositoryImpl.class
		
		)//por default asi si manejamos varios abra que hacer varios paquetes--------entityManagerFactoryRef="entityManagerFactory"
		*/
public class IeblPersistenceConfig
{

	/*
	@Override
	public Executor getAsyncExecutor() {
		 	ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	        executor.setCorePoolSize(2);
	        executor.setMaxPoolSize(5);
	        executor.setQueueCapacity(100);
	        executor.setThreadNamePrefix("MyExecutor-");
	        executor.initialize();
	        return executor;
	}
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new AsyncHandler();
	}
	@Bean
	@Primary
	public DriverManagerDataSource oracleCrmDataSource(){
		DriverManagerDataSource dmds=new DriverManagerDataSource();
		dmds.setDriverClassName(DataSourceProperties.CRM_DRIVER);
		dmds.setUrl(DataSourceProperties.CRM_URL);
		dmds.setUsername(DataSourceProperties.CRM_USER);
		dmds.setPassword(DataSourceProperties.CRM_PASSWORD);
		return dmds;
		
	}
    */
}
