package com.iebl.app.config;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

public class AsyncHandler implements AsyncUncaughtExceptionHandler{
	

	@Override
	public void handleUncaughtException(Throwable ex, Method method,
			Object... params) {
		
	Logger.getLogger(this.getClass()).error("Unhandle Exception", ex);	
	}

}
