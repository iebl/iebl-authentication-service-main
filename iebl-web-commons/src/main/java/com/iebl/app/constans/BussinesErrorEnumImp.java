package com.iebl.app.constans;

import com.iebl.common.error.BussinesErrorEnum;

/**
 * Enumerador para las excepciones
 * 
 * @author vcamach o
 *
 */
public enum BussinesErrorEnumImp implements BussinesErrorEnum{
	VALIDATE_REQ_DATA("VG000"),
	VALIDATE_EMAIL("VL001"),
	VALIDATE_KEY("VL002");

	private String code;
	private BussinesErrorEnumImp(String code0  ) {
		code=code0;
	}



	@Override
	public String getCode() {
		return code;
	}


}
