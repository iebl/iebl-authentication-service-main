package com.iebl.app.vo;

import java.io.Serializable;

public class LoginReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4278786910798551906L;
	private String email;
	private String password;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
