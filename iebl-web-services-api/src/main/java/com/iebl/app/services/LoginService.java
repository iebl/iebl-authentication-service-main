package com.iebl.app.services;

import com.iebl.app.vo.LoginReq;
import com.iebl.vo.RequestVO;
import com.iebl.vo.ResponseVO;

public interface LoginService {
	/**
	 * regresa la utorizacion del login
	 * @param logInReq
	 * @return
	 */
	ResponseVO<String> logIn(RequestVO<LoginReq> logInReq);

}
