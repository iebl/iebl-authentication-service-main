package com.iebl.app.rest;

import com.iebl.app.vo.LoginReq;
import com.iebl.vo.RequestVO;
import com.iebl.vo.ResponseVO;


public interface LogInRest {

	/**
	 * 
	 * @param request
	 * @return
	 */
	ResponseVO<String> logIn(RequestVO<LoginReq> request);
	
}