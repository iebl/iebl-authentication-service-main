package com.iebl.app.services.impl;

import org.springframework.stereotype.Service;

import com.iebl.app.constans.BussinesErrorEnumImp;
import com.iebl.app.model.UserDO;
import com.iebl.app.services.LoginService;
import com.iebl.app.services.transformer.LoginTransformer;
import com.iebl.app.services.util.ServiceTransformer;
import com.iebl.app.vo.LoginReq;
import com.iebl.common.utils.ValidationUtil;
import com.iebl.vo.RequestVO;
import com.iebl.vo.ResponseVO;


/**
 * CustoermService Example
 * 
 * @author vn04d25
 */
@Service
public class LogInServiceImpl implements LoginService {


	//@Autowired
	//private UserRepository userDAO;

	@Override
	public ResponseVO<String> logIn(RequestVO<LoginReq> logInReq) {
		//TODO
		ResponseVO<String> responseVO=new ResponseVO<String>();
		//validaciones
		ValidationUtil.validateAndThrows(logInReq.getReqData(),BussinesErrorEnumImp.VALIDATE_REQ_DATA );
		ValidationUtil.validateAndThrows(logInReq.getReqData().getEmail(),BussinesErrorEnumImp.VALIDATE_EMAIL );
		ValidationUtil.validateAndThrows(logInReq.getReqData().getPassword(),BussinesErrorEnumImp.VALIDATE_KEY );
		/*
		responseVO=ServiceTransformer.fromDOToResponse(responseVO,userDAO.findByEmailAndPassword(),LoginTransformer.class);
		*/
		UserDO userDO=new UserDO();
		userDO.setLastname("MY LAST NAME");
		responseVO=ServiceTransformer.fromDOToResponse(responseVO,userDO,LoginTransformer.class);
		

		return responseVO;
	}



	
	
	

}
