package com.iebl.app.services.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

import com.iebl.app.constans.BussinesErrorEnumImp;
import com.iebl.app.rep.ext.ExtendedRepository;
import com.iebl.base.BaseTransformer;
import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.utils.PaginationUtil;
import com.iebl.common.utils.TransformUtil;
import com.iebl.common.utils.ValidationUtil;
import com.iebl.vo.PaginationVO;
import com.iebl.vo.RequestVO;
import com.iebl.vo.ResponseVO;
import com.iebl.vo.ext.ExtendedSpecicationBean;

public class ServiceTransformer {
	/**
	 * Convierte con ayuda de un transformador de una lista de entrada a una lista de salida para ResponseVO
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static  <O,I,TR extends BaseTransformer<O, I>>  ResponseVO<List<O>>  fromDOToResponse(ResponseVO<List<O>> responseVO, List<I> dOList, Class<TR> claseTransformer,Long totalItems){
		List<O> list=new ArrayList<O>();
		TransformUtil.transformListTransformer(claseTransformer, dOList, list);	
		responseVO.setRespData(list);
		TransformUtil.transformResponseFromData(responseVO, totalItems);
		return responseVO;
	}
	
	/**
	 * Convierte con ayuda de un transformador de una lista de entrada a una lista de salida para ResponseVO
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static  <O,I,TR extends BaseTransformer<O, I>>  ResponseVO<List<O>>  fromDOToResponse(ResponseVO<List<O>> responseVO, Page<I> page, Class<TR> claseTransformer){

			return fromDOToResponse(responseVO,
					page.getContent(),
					claseTransformer, 
					page.getTotalElements());

	}
	
	/**
	 * Obtiene la transformacion un  response cuenta el indice 0 pero no cuenta si el indice no es cero, ademas pagina las entradas.
	 * @param responseVO
	 * @param spe
	 * @param request
	 * @param claseTransformer
	 * @param repo
	 * @return
	 */
	
	public static  <O,I,TR extends BaseTransformer<O, I>,ID extends Serializable>  ResponseVO<List<O>>  fromDOToResponsePaginateAndCountOnlyZeroIndex(ResponseVO<List<O>> responseVO, Specification<I> spe,RequestVO<?> request, Class<TR> claseTransformer,ExtendedRepository<I,ID> repo){
		 ExtendedSpecicationBean<I> expe=new ExtendedSpecicationBean<I>();
		 expe.setSpecification(spe);
		 return fromDOToResponsePaginateAndCountOnlyZeroIndex(responseVO, expe, request, claseTransformer, repo);
	}
	/**
	 * Obtiene la transformacion un  response cuenta el indice 0 pero no cuenta si el indice no es cero, ademas pagina las entradas pero recive un Bean de Especificacion Extendida.
	 * @param responseVO
	 * @param spe
	 * @param request
	 * @param claseTransformer
	 * @param repo
	 * @return
	 */
	
	public static  <O,I,TR extends BaseTransformer<O, I>,ID extends Serializable>  ResponseVO<List<O>>  fromDOToResponsePaginateAndCountOnlyZeroIndex(ResponseVO<List<O>> responseVO, ExtendedSpecicationBean<I> exspe,RequestVO<?> request, Class<TR> claseTransformer,ExtendedRepository<I,ID> repo){
		paginateAndCountZeroIndexValidations(request.getPaginationVO());
		if(PaginationUtil.isFirstIndex(request.getPaginationVO())){
			return fromDOToResponse(
					responseVO,
					repo.findAll(exspe,ServiceTransformer.getPageable(request.getPaginationVO())),
					 claseTransformer
					);
		}else{
			return fromDOToResponse(
					responseVO,
					repo.findAllNoCount(exspe,ServiceTransformer.getPageable(request.getPaginationVO())),
					 claseTransformer
					);
		}
	}
	
	
	private static void paginateAndCountZeroIndexValidations(PaginationVO pagReq){
		ValidationUtil.validateAndThrows(pagReq, BussinesVsmartCommonErrorEnumImp.PAGINATION_REQUEST_REQUIRED);
		ValidationUtil.validateAndThrows(pagReq.getIndex(), BussinesVsmartCommonErrorEnumImp.PAGINATION_REQUEST_REQUIRED);
		ValidationUtil.validateAndThrows(pagReq.getSize(), BussinesVsmartCommonErrorEnumImp.PAGINATION_REQUEST_REQUIRED);
	}
	
	/**
	 * Convierte con ayuda de un transformador de una lista de entrada a una lista de salida para ResponseVO
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static  <O,I,TR extends BaseTransformer<O, I>>  ResponseVO<List<O>>  fromDOToResponse(ResponseVO<List<O>> responseVO, List<I> dOList, Class<TR> claseTransformer){
	
		return fromDOToResponse(responseVO, dOList, claseTransformer,null);
	}
	
	/**
	 * Convierte con ayuda de un transformador de un Objeto T a un ResponseVO<T>
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static  <O,I,TR extends BaseTransformer<O, I>>  ResponseVO<O>  fromDOToResponse(ResponseVO<O> responseVO ,I dO, Class<TR> claseTransformer,Long totalItems){
		
	
			BaseTransformer<O, I> baseTransformer=TransformUtil.getNewInstance(claseTransformer);
			responseVO.setRespData(baseTransformer.getResponse(dO));
			TransformUtil.transformResponseFromData(responseVO,totalItems);
		
		return responseVO;
	}
	
	/**
	 * llena el Response<String> con un ok
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static   ResponseVO<String>  toResponseOk(ResponseVO<String> responseVO ){
		//si no hay header o el header es success.
		if(responseVO.isSuccess()){
			responseVO.setRespData("OK");
			TransformUtil.transformResponseFromData(responseVO,null);
		}
		return responseVO;
	}
	
	/**
	 * Convierte con ayuda de un transformador de un Objeto T a un ResponseVO<T>
	 * @param dOList
	 * @param clase
	 * @param totalItems
	 * @return
	 */
	public static  <O,I,TR extends BaseTransformer<O, I>>  ResponseVO<O>  fromDOToResponse(ResponseVO<O> responseVO ,I dO, Class<TR> claseTransformer){
		return fromDOToResponse(responseVO,dO,claseTransformer,null);
	}
	/**
	 * Obteiene Spring Pageable de Request
	 * Si tiene orderType agrega el Sort
	 * @param request
	 * @return
	 */
	public static Pageable getPageable(RequestVO<?> request){
		Pageable pageable=null;
		if(ValidationUtil.isNotNull(request)){
			return ServiceTransformer.getPageable(request.getPaginationVO());
		}
		
		return pageable;
		
	}
	/**
	 * Obtiene Spring Pageable de paginationRequest
	 * 
	 * @param PaginationVO
	 * @return
	 */
	public static Pageable getPageable(PaginationVO PaginationVO){
		Pageable pageable=null;
		if(ValidationUtil.isNotNull(PaginationVO)){
				pageable=new PageRequest(PaginationVO.getIndex().intValue(), PaginationVO.getSize().intValue());			
		}
		
		return pageable;
		
	}
	/**
	 * Obtiene Spring pageable de RequestVO y orden de Sort
	 * @param request
	 * @param sort
	 * @return
	 */
	public static Pageable getPageable(RequestVO<?> request,String properties){
		Pageable pageable=null;
		if(ValidationUtil.isNotNull(request)){
			return getPageable(request.getPaginationVO(),properties);
		}
		return pageable;
	}
/**
 * Obtiene Spring pageable de PaginationVO y orden de Sort
 * @param PaginationVO
 * @param sort
 * @return
 */
	public static Pageable getPageable(PaginationVO PaginationVO,String properties){
		Pageable pageable=null;
		Sort sort;
		if(ValidationUtil.isNotNull(PaginationVO)){
			if(ValidationUtil.isNotNull(PaginationVO.getOrderType())){
				sort=new Sort(Direction.valueOf(PaginationVO.getOrder()),properties);
			}else{
				sort=new Sort(properties);
			}
			pageable=new PageRequest(PaginationVO.getIndex().intValue(), PaginationVO.getSize().intValue(),sort);
		}
		return pageable;
	}
	/**
	 *  Obtiene Spring pageable de PaginationVO y ordenado por Direction y los campos prop
	 * @param request
	 * @param prop
	 * @return
	 */
	public static Pageable getPageable(RequestVO<?> request,Direction direction,String... prop){
		Pageable pageable=null;
		if(ValidationUtil.isNotNull(request)){
			return getPageable(request.getPaginationVO(),new Sort(direction,prop));
		}
		return pageable;
	}
	
	/**
	 * Obtiene Spring pageable de  request y el orden Sort
	 * @param request
	 * @param sort
	 * @return
	 */
	public static Pageable getPageable(RequestVO<?> request,Sort sort){
		Pageable pageable=null;
		if(ValidationUtil.isNotNull(request)){
			return getPageable(request.getPaginationVO(),sort);
		}
		return pageable;
	}
/**
 * Obtiene Spring pageable de PaginationVO y el orden Sort
 * @param paginationRequest
 * @param sort
 * @return
 */
	private static Pageable getPageable(PaginationVO paginationRequest,Sort sort) {
		Pageable pageable=null;

		if(ValidationUtil.isNotNull(paginationRequest)){
		
			pageable=new PageRequest(paginationRequest.getIndex().intValue(), paginationRequest.getSize().intValue(),sort);
		}
		return pageable;
	}
	


}
