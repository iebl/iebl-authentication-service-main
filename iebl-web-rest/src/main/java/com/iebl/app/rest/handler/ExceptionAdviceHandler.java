package com.iebl.app.rest.handler;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.iebl.common.utils.TransformUtil;
import com.iebl.vo.ResponseVO;


@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON)
public class ExceptionAdviceHandler {
	private static final Logger LOG = Logger.getLogger(ExceptionAdviceHandler.class);
	@ResponseBody
	@ExceptionHandler({ Throwable.class })
	public Object handleException(Throwable exception, WebRequest request) {
		return handleException(exception);
	}

	/**
	 * Se puede reutilizar el manejo de excepcion por medio de este metodo
	 * 
	 * @param exception
	 * @param url
	 * @return
	 */
	public ResponseVO<String> handleException(Throwable exception) {
		ResponseVO<String> zResponse;
		zResponse = new ResponseVO<String>();
		LOG.error("Iebl Exception ", exception);
		TransformUtil.transformOnException(zResponse, exception);
			return zResponse;
		

	}

}