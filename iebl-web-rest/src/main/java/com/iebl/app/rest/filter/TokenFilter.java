package com.iebl.app.rest.filter;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iebl.app.rest.handler.ExceptionAdviceHandler;
import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.error.CrmGenericException;
import com.iebl.common.utils.ResourceUtil;




@Configuration
public class TokenFilter implements Filter {

	private static final String EXCLUDE_METOD_OPTIONS = "OPTIONS";
	private static final String PROPERTIES_URLS_TO_EXCLUDE = "urls-to-exclude-form-token-validation.properties";

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
	
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String authorization=httpRequest.getHeader("Authorization");
		//TokenValidationClientImpl vtr=null;
		//TODO
		try {
			if (!excludeUri(httpRequest.getRequestURI().toString())&&!httpRequest.getMethod().equals(EXCLUDE_METOD_OPTIONS)) {
				//vtr = new TokenValidationClientImpl();
				//if (vtr.validateToken(authorization)) {
				if(true){
					filterChain.doFilter(request, response);
				} else {
					
					CrmGenericException.throwExcepcion(BussinesVsmartCommonErrorEnumImp.TOKEN_INVALID);
				}
			
			} else {
			
				filterChain.doFilter(request, response);
			}
		} catch (Throwable e) {
			handleExceptionResponse(e, request, response);
		}
	}
	
	private boolean excludeUri(String uri) {
		boolean exclude = false;
		Properties properties = ResourceUtil.loadProperties( PROPERTIES_URLS_TO_EXCLUDE );
		for(Entry<Object, Object> property : properties.entrySet()) {
			if(uri.contains(property.getValue().toString())) {
				exclude = true;
			}
        }
		return exclude;
		
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	private void handleExceptionResponse(Throwable e, ServletRequest request, ServletResponse response) {

		Object objresponse;
		String jsonResponse;
		ExceptionAdviceHandler eah = new ExceptionAdviceHandler();
		ObjectMapper om = new ObjectMapper();
		response.setContentType("application/json");
		try {
			objresponse = eah.handleException(e);
			jsonResponse = om.writeValueAsString(objresponse);
			response.getWriter().print(jsonResponse);
			response.getWriter().flush();
		} catch (IOException e1) {
			CrmGenericException.throwExcepcion(BussinesVsmartCommonErrorEnumImp.TOKEN_VALIDATION_GENERIC_FAILURE);
		}
	}

}
