package com.iebl.app.rest.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.DispatcherServlet;



import com.iebl.common.utils.ResourceUtil;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Exaple for SpringBoot Configuration
 * 
 * @author vn04d25
 */
@SpringBootApplication(scanBasePackages = "com.iebl")
@EnableAspectJAutoProxy
@EnableSwagger2
public class IeblWebConfig extends SpringBootServletInitializer {

    private static final String MICROSERVICE_CONTACT_EMAIL = "microservice.contact.email";
    private static final String MICROSERVICE_CONTACT_URL = "microservice.contact.url";
    private static final String MICROSERVICE_CONTACT_NAME = "microservice.contact.name";
    private static final String MICROSERVICE_DESCRIPTION = "microservice.description";
    private static final String MICROSERVICE_TITLE = "microservice.title";
    private static final String PROJECT_VERSION = "project.version";
    public static final String PROPERTIES_CONFIG = "config.properties";

    /**
     * Register the dispatcherServlet and loads on startup
     * 
     * @param dispatcherServlet
     * @return
     */
    @Bean
    public ServletRegistrationBean dispatcherServletRegistration( DispatcherServlet dispatcherServlet ) {
        ServletRegistrationBean registration = new ServletRegistrationBean( dispatcherServlet );
        registration.setLoadOnStartup( 1 );
        return registration;
    }

    /**
     * Starter for SprimngBoot Application
     * 
     * @param args
     */
    public static void main( String... args ) {
        SpringApplication.run( IeblWebConfig.class, args );
    }

    @Override
    protected SpringApplicationBuilder configure( SpringApplicationBuilder application ) {
        return application.sources( IeblWebConfig.class );
    }

    /**
     * CommnadLinnerRunner
     * 
     * @param ctx
     * @return
     */
    @Bean
    public CommandLineRunner commandLineRunner( final ApplicationContext ctx ) {
        return new CommandLineRunner(){

            @Override
            public void run( String... args ) throws Exception {
                String[] beanNames = ctx.getBeanDefinitionNames();
                Arrays.sort( beanNames );
            }
        };
    }

    /**
     * Produces the information required by Swagger2 to render the Swagger-UI for the REST services published by the
     * application.
     * 
     * @return
     */
    @Bean
    public Docket productApi() {
        return new Docket( DocumentationType.SWAGGER_2 ).select()
                .apis( RequestHandlerSelectors.basePackage( "com.iebl.app.rest.impl" ) )
                .paths( PathSelectors.any() ).build().apiInfo( buildApiInfo() ).useDefaultResponseMessages( false );
    }

    @SuppressWarnings("rawtypes")
    private ApiInfo buildApiInfo() {
        Properties properties = ResourceUtil.loadProperties( PROPERTIES_CONFIG );
        return new ApiInfo( properties.getProperty( MICROSERVICE_TITLE ),
                properties.getProperty( MICROSERVICE_DESCRIPTION ), properties.getProperty( PROJECT_VERSION ),
                "N/A",
                new Contact( properties.getProperty( MICROSERVICE_CONTACT_NAME ),
                        properties.getProperty( MICROSERVICE_CONTACT_URL ),
                        properties.getProperty( MICROSERVICE_CONTACT_EMAIL ) ),
                "N/A"/* license */, "N/A"/* licenseUrl */, new ArrayList<VendorExtension>() );
    }

}
