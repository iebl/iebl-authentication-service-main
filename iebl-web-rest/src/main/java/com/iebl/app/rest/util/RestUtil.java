package com.iebl.app.rest.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.error.CrmGenericException;


public class RestUtil {
	
	public static <T,R> T getPostTrust(R requestClass,Class<T> responseClass,String url){
		RestTemplate restTemplate;
		T result=null;
		try {
			restTemplate = restTemplate();
			HttpEntity<R> request = new HttpEntity<R>(requestClass);
			result=restTemplate.postForObject(url, request,responseClass);	
		} catch (Throwable e) {
			throw new CrmGenericException(e, BussinesVsmartCommonErrorEnumImp.TOKEN_VALIDATION_GENERIC_FAILURE);
		} 
		
		return result;
	}
	
	/**
	 * Esquiva la certificacion ssl solo para fuentes confiables
	 * @return
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	private static RestTemplate restTemplate()
			throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
				.loadTrustMaterial(null, acceptingTrustStrategy)
				.build();

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
				.setSSLSocketFactory(csf)
				.build();

		HttpComponentsClientHttpRequestFactory requestFactory =
				new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		return restTemplate;
	}

}
