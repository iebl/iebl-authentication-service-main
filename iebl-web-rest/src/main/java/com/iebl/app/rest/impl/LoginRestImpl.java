package com.iebl.app.rest.impl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ResponseHeader;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.iebl.app.rest.LogInRest;
import com.iebl.app.services.LoginService;
import com.iebl.app.vo.LoginReq;
import com.iebl.vo.RequestVO;
import com.iebl.vo.ResponseVO;




@RestController
@RequestMapping("/authorization")
public class LoginRestImpl implements LogInRest {
	
	@Autowired
	private LoginService loginService;


	@Override
	@CrossOrigin
	@RequestMapping(path = "/logIn", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	@ApiOperation(value = "login and get token auth ", notes = "Response Codes:<br/>"
         + "No existen resultados para su consulta."
       )
	//TODO eliminar solo es ejemplo
	//Agregaa el token al header en swangger.
	@ApiImplicitParams({
           @ApiImplicitParam(name = "authorization", value = "Token not needed here", paramType = "header"),
     
   })

	public ResponseVO<String> logIn(@RequestBody @Valid RequestVO<LoginReq> request) {

		return loginService.logIn(request);
	}
	
}
