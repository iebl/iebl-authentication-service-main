package com.iebl.jasper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;

import org.olap4j.impl.Base64;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;


public class JasperReportUtil {
	private static final Integer HEADER_ROW = 7;
	/**
	 * Crea un byteArray con el template.jasper los parametros y el objeto que se le pasan 
	 * @param jasperTemplate
	 * @param parameters
	 * @param dataColection
	 * @return
	 * @throws JRException
	 */
	
	private JasperReportUtil() {
		
	}
	
	public static byte[] createExcelByteArrayFromObject(InputStream jasperTemplate,Map<String, Object> parameters,Collection<?> dataColection) throws JRException{
		
		ByteArrayOutputStream byteArray=new ByteArrayOutputStream();//arreglo en memoria para el documento que se genera
		OutputStream xlsReport=byteArray;//ouputStream de el documento en memoria

		JasperPrint print=createJasperPrintFromObject(jasperTemplate, parameters, dataColection);   	
		JRXlsExporter exporter = new JRXlsExporter(); //exporter para crear el excel
		exporter.setExporterInput(new SimpleExporterInput(print));// se le pasa el print de jasper
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));//se le dice donde genere el documento en este caso en memoria
		//configuracion
		setConfiguration(exporter);
		exporter.exportReport();// se genera el reporte


		return byteArray.toByteArray();// se codifica en base 64 y se regresa

	}

	/**
	 * Crea un archivo excel con el template.jasper los parametros, el objeto que se le pasan e indicandole el archivo donde se deposita
	 * @param jasperTemplate
	 * @param parameters
	 * @param dataColection
	 * @param fileOutputUrl
	 * @throws JRException
	 */

	public static void createExcelFileFromObject(InputStream jasperTemplate,Map<String, Object> parameters,Collection<?> dataColection,String fileOutputUrl) throws JRException{
		
		JasperPrint print=createJasperPrintFromObject(jasperTemplate, parameters, dataColection);
		JRXlsExporter exporter = new JRXlsExporter(); //exporter para crear el excel
		exporter.setExporterInput(new SimpleExporterInput(print));// se le pasa el print de jasper
		File file=new File(fileOutputUrl);
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));//se le dice donde genere el documento en este caso en documento
		//configuracion
		setConfiguration(exporter);
		exporter.exportReport();// se genera el reporte
	}
	/**
	 * Crea el jasper print
	 * @param jasperTemplate
	 * @param parameters
	 * @param dataColection
	 * @return
	 * @throws JRException
	 */
	private static JasperPrint createJasperPrintFromObject(InputStream jasperTemplate,Map<String, Object> parameters,Collection<?> dataColection) throws JRException{

		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		return JasperFillManager.fillReport(			
				jasperTemplate, //archivo .jasper
				parameters, //parametros
				new JRBeanCollectionDataSource(dataColection));// objeto con los datos del reporte
	}
/**
 * Se configura el xls
 * @param exporter
 */
	private static void setConfiguration(JRXlsExporter exporter){
		//configuracion
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet(false);
		configuration.setDetectCellType(false);
		configuration.setForcePageBreaks(false);
		configuration.setRemoveEmptySpaceBetweenRows(true);
		configuration.setIgnorePageMargins(true);

		configuration.setFreezeRow(HEADER_ROW);
		//The other properties you like to set
		exporter.setConfiguration(configuration);// se seta la configuracion
	}
	
	public static byte[] stringBase64ToByteArray(String stringData){
		return Base64.decode(stringData);
	}
	
	public static String byteArrayToBase64(byte[] data){
		return Base64.encodeBytes(data);
	}
}
