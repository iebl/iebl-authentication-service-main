package com.iebl.jasper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

import org.apache.poi.util.IOUtils;
import org.junit.Test;



public class JasperReportUtilTestCase {
	
	private static final String LOGO_URL = "/jasper/crmLogo.jpg";
	private static final String JASPER_FILE_URL = "/jasper/ReporteGeneralME.jasper";
	private static final String OUTPU_FILE_URL = "C:/temp/jaspertest/ReporteGeneral.xls";

	


	
	private Map<String, Object> getParameters() throws IOException{
		Map<String, Object> parameters=new HashMap<String, Object>();
		parameters.put("REPORT_TITLE", "Reporte 0-45");
		
		parameters.put("REPORT_LOGO",IOUtils.toByteArray(JasperReportUtil.class.getResourceAsStream(LOGO_URL)));
	
		return parameters;
	}

	public void createExcelStringBase64FromObject(){
		InputStream input = JasperReportUtil.class.getResourceAsStream(JASPER_FILE_URL);
		Collection<?> obj = new ArrayList<String>();
		String string64;
		
		try {
			string64 = JasperReportUtil.byteArrayToBase64(JasperReportUtil.createExcelByteArrayFromObject(input,getParameters(),obj));
			System.out.println(string64);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//En looper no encuentra el archivo @Test
	public void createExcelFileFromObject(Collection<?> obj) throws JRException, IOException{
		InputStream input = JasperReportUtil.class.getResourceAsStream(JASPER_FILE_URL);
		System.out.println(input);
		JasperReportUtil.createExcelFileFromObject(input,getParameters() ,obj, OUTPU_FILE_URL);
	}
	
	
	@Test
	public void stringBase64ToByteArray() throws JRException, IOException{
		JasperReportUtil.stringBase64ToByteArray("hola");
	}
	
	
}
