package com.iebl.base;



/**
 * 
 * @author vcamacho
 *
 * @param <I> Bean entrada
 * @param <O> Bean Salida
 */
public interface BaseTransformer <O,I> {
/**
 * Transforma de un Bean de entrada a uno de Salida
 * @param in Bean de Entrada
 * @return
 */
	public  O getResponse(I in) ;
	
}
