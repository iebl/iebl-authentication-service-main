package com.iebl.common.error;

public interface BussinesErrorEnum {

	String getCode();
	String name();

}
