package com.iebl.common.error;




/**
 * 
 * @author vcamacho
 */
public class CrmGenericException extends RuntimeException {

	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1918283082404410227L;
	
	private final BussinesErrorEnum errorEnum;
	
	private Object[] params;



	public CrmGenericException(Throwable e,BussinesErrorEnum errorEnum) {
		super.initCause(e);
		this.errorEnum=errorEnum;
	}
	public CrmGenericException(Throwable e,BussinesErrorEnum errorEnum,Object... params0) {
		super.initCause(e);
		this.errorEnum=errorEnum;
		this.params=params0;
	}
	public BussinesErrorEnum getErrorEnum() {
		return errorEnum;
	}
	/**
	 * lanza una excepcionGenerica
	 * @param e
	 */
	public static void throwExcepcion(Throwable e){
		throw new CrmGenericException(e, BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION);
	}
	/**
	 * lanza una excepcion del tipo especificado
	 * @param e
	 * @param errorEnum
	 */
	public static void throwExcepcion(Throwable e,BussinesErrorEnum errorEnum){
		throw new CrmGenericException(e,errorEnum);
	}
	
	
	public static void throwExcepcion(Throwable e,BussinesErrorEnum errorEnum,Object... params){
		
		throw new CrmGenericException(e,errorEnum,params);
	}
	
	public static void throwExcepcion(BussinesErrorEnum errorEnum,Object... params){
		Throwable e=new Throwable("BusinesException");
		throwExcepcion(e,errorEnum,params);
	}
	
	public static void throwExcepcion(BussinesErrorEnum errorEnum){
		Throwable e=new Throwable("BusinesException");
		throwExcepcion(e,errorEnum);
	}
	public Object[] getParams() {
		return params;
	}

}
