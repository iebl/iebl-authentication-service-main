package com.iebl.common.error;

/**
 * Enumerador para las excepciones
 * 
 * @author vcamach o
 *
 */
public enum BussinesVsmartCommonErrorEnumImp implements BussinesErrorEnum{
	UNEXPECTED_EXCEPTION("EG000"),
	DATA_NOT_EXIST("EG001"),
	PAGINATION_REQUEST_REQUIRED("EG0012"),
	PARSING_DATE_ERROR("EG003"),
	FIND_QUERY_NOT_FOUND("EG004"),
	TOKEN_VALIDATION_GENERIC_FAILURE("EG005"),
	TOKEN_INVALID("EG005"),
	REST_INVOCATION_ERROR("ES000"),
	EXTERNAL_REST_SERVICE_ERROR("ES001");



	private String code;
	private BussinesVsmartCommonErrorEnumImp(String code0  ) {
		code=code0;
	}



	@Override
	public String getCode() {
		return code;
	}


}
