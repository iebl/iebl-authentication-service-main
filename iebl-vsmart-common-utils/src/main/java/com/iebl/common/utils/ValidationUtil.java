package com.iebl.common.utils;

import java.util.Collection;
import java.util.List;

import com.iebl.common.error.BussinesErrorEnum;
import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.error.CrmGenericException;
import com.iebl.vo.PaginationVO;
import com.iebl.vo.ResponseVO;










/**
 * Utileria para validaciones
 * 
 * @author vcamacho
 *
 */
public class ValidationUtil {
	/**
	 * Hiden Utility constructor
	 */
	private ValidationUtil() {
		// Hiden Utility constructor
	}

	/**
	 * Valida si existe resultados agregando true a success caso contrario
	 * agrega una excepcion y false
	 * 
	 * @param response
	 */
	public static boolean validateExistData(ResponseVO<?> response) {
		Object data=response.getRespData();
		if (ValidationUtil.isNull(data)
				|| (data instanceof List<?> && ((List<?>) data).isEmpty())) {
			setResponseVoError(response, BussinesVsmartCommonErrorEnumImp.DATA_NOT_EXIST,null);
			return false;
		} else {
			
			return true;
		}

	}



	/**
	 * validate a object or collection or empty string if is null throws  Bussiness exception
	 * @param data
	 * @param error
	 */
	public static void validateAndThrows(Object data,BussinesErrorEnum error){
		if (ValidationUtil.isNull(data)
				|| (data instanceof List<?> && (ValidationUtil.isNullOrEmpty((List<?>) data))) ||
				(data instanceof String && ValidationUtil.isNullOrEmpty((String)data))){
			CrmGenericException.throwExcepcion(error);
		} 
	}
	
	
	/**
	 * Valida si la lista no es nula o vacia y agrega excepcion de serlo
	 * 
	 * @param response
	 * @param parameter
	 * @param error
	 * @return
	 */
	public static boolean validateList(ResponseVO<?> response, List<?> list,
			BussinesErrorEnum error,Object[] params) {
		
		if (isNullOrEmpty(list)) {
			setResponseVoError(response, error,params);
			return false;
		}
		return true;
	}

	/**
	 * Valida si el parametro no es nulo o vacio y agrega una excepcion especifica de no
	 * serlo
	 * 
	 * @param response
	 * @param parameter
	 * @param error
	 * @return
	 */
	public static boolean validateString(ResponseVO<?> response,
			String parameter, BussinesErrorEnum error,Object[] params) {
		if (isNullOrEmpty(parameter)) { 
			setResponseVoError(response, error,params);
			return false;
		}
		return true;
	}
	

	
	
	public static <T> boolean validateZeusParameter(ResponseVO<T> response,
			Object parameter, BussinesErrorEnum error,Object[] params) {
		if (ValidationUtil.isNull(parameter) || (parameter instanceof List<?> && ((List<?>) parameter).isEmpty())) {
			response.setSuccess(false);		
			TransformUtil.setErrors(response.getExceptionDetailList(), error,params);
			return false;
		}
		return true;
	}

	/**
	 * Setea un error para el responseVO
	 * 
	 * @param response
	 * @param error
	 */
	public static void setResponseVoError(ResponseVO<?> response,
			BussinesErrorEnum error,Object[] params) {
		response.setSuccess(false);
		TransformUtil.setErrors(response.getExceptionDetailList(), error,params);
	}

	/**
	 * Valida y setea el error DATA_NOT_EXIST cuando el numero total de
	 * resultados es cero
	 * 
	 * @param responseVO
	 * @param totalReports
	 */

	public static boolean validateDataCount(ResponseVO<?> responseVO, Long count,PaginationVO pagination) {
		if (!ValidationUtil.isNotNull(count)||count == 0) {
			
		
			TransformUtil.setErrors(responseVO.getExceptionDetailList(), BussinesVsmartCommonErrorEnumImp.DATA_NOT_EXIST,null);
		
			responseVO.setSuccess(false);
			return false;
		}else{
			pagination.setTotalSize(count);
			responseVO.setPaginationVO(pagination);
		
		}
		return true;
	}
	

	/**
	 * regresa true si el objeto no es nulo
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isNotNull(Object object) {
		return !ValidationUtil.isNull(object);
	}
	/**
	 * regresa true si el objeto es nulo
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isNull(Object object){
		return object==null;
	}

	/**
	 * regresa true si la coleccion no es nula ni vacia
	 * 
	 * @param col
	 * @return
	 */
	public static boolean isNotNullOrEmpty(Collection<?> col) {
		return isNotNull(col) && !col.isEmpty();
	}
	
	public static boolean isNullOrEmpty(Collection<?> col){
		return !isNotNullOrEmpty(col);
	}
	/**
	 * regresa true si la coleccion no es nula ni vacia
	 * 
	 * @param col
	 * @return
	 */
	public static boolean isNotNullOrEmpty(String cadena) {
		return isNotNull(cadena)&&!cadena.isEmpty();
	}
	/**
	 * regresa true si la coleccion no es nula ni vacia
	 * 
	 * @param col
	 * @return
	 */
	public static boolean isNullOrEmpty(String cadena) {
		return !isNotNullOrEmpty(cadena);
	}
	

	/**
	 * true si es 0
	 * @param number
	 * @return
	 */
	public static boolean isZero(Long number){
		return number==0;
	}
	/**
	 * true si no es 0
	 * @param number
	 * @return
	 */
	public static boolean isNotZero(Long number){
		return!isZero(number);
	}
	/**
	 * true si no es null y no es 0
	 * @param number
	 * @return
	 */
	public static boolean isNotNullAndNotZero(Long number){
		return isNotNull(number)&&isNotZero(number);
	}
	/**
	 * true si no es null y es 0
	 * @param number
	 * @return
	 */
	public static boolean isNotNullAndZero(Long number){
		return isNotNull(number)&&isZero(number);
	}
	

	
}