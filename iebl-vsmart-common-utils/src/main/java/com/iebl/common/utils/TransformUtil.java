package com.iebl.common.utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;








import com.google.gson.Gson;
import com.iebl.base.BaseTransformer;
import com.iebl.common.error.BussinesErrorEnum;
import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.error.CrmGenericException;
import com.iebl.vo.ExceptionDetail;
import com.iebl.vo.ResponseVO;



public class TransformUtil {
	private static final Properties ERROR_PROPERTIES = ResourceUtil.loadProperties( "responseError.properties" );
	
	private static final String STRING_NO = "No";
	private static final String STRING_YES = "Si";
	private static final String STRING_SPACE = " ";


	/**
	 * Private hiden util constructor
	 */
	private TransformUtil() {
		// Private hiden util constructor
	}

	public static BigInteger getBigInteger(Integer integer) {
		BigInteger result = null;
		if (integer != null) {
			result = new BigInteger(integer.toString());
		}
		return result;
	}
	
	
	

	/**
	 * Convierte de calendar a gregorian calendar
	 * 
	 * @param cal
	 * @return
	 */
	public static XMLGregorianCalendar getXMLCalendar(Calendar cal) {
		
		if (cal != null) {
			return TransformUtil.getXMLCalendar(cal.getTime());
		}
		return null;
	}

	/**
	 * Convierte de xmlGregorianCalendar a Date
	 * 
	 * @param xmlGregorianCalendar
	 * @return
	 */
	public static Date getDate(XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar != null) {
			return xmlGregorianCalendar.toGregorianCalendar().getTime();
		}
		return null;
	}

	/**
	 * Convierte de Date a gregorian calendar
	 * 
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException 
	 */
	public static XMLGregorianCalendar getXMLCalendar(Date date)  {
		GregorianCalendar gc;
		XMLGregorianCalendar xgc=null;
		if (date != null) {
			gc = new GregorianCalendar();
			gc.setTime(date);
			try {
				xgc= DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			} catch (DatatypeConfigurationException e) {
				CrmGenericException.throwExcepcion(e);
			}
			
		}
		return xgc;
	}

	

	/**
	 * Busca un tipo de excepcion en la traza si lo encuentra la regresa si no
	 * regresa nulo
	 * 
	 * @param classe
	 * @param origExc
	 * @return
	 */
	public static <T extends Throwable> T searchException(Class<T> classe,
			Throwable origExc) {
		boolean exit = false;
		Throwable currentE = origExc;
		while (!exit) {
			if (ValidationUtil.isNotNull(currentE)) {
				if (currentE.getClass().equals(classe)) {
					return classe.cast(currentE);
				} else {
					currentE = currentE.getCause();
				}
			} else {
				exit = true;
			}
		}
		return null;
	}




	
	
	public static <T> void transformResponseFromData(ResponseVO<T> response,Long totalItems){
		
		if(ValidationUtil.validateExistData(response)){
			if(ValidationUtil.isNotNull(totalItems)) {
				response.getPaginationVO().setTotalSize(totalItems);
			}
			response.setSuccess(true);
		}
	}
	
	

	
	
	
	
	public static<T> void  transformOnException(ResponseVO<T> response, Throwable e) {
		
		Object[] params=null;
		CrmGenericException crmGenericException = searchException(
				CrmGenericException.class, e);
		BussinesErrorEnum bussinesErrorEnum;
		
		if (ValidationUtil.isNull(crmGenericException)) {
	
			bussinesErrorEnum=BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION;
			
		} else{
			bussinesErrorEnum=crmGenericException.getErrorEnum();
			params=crmGenericException.getParams();
		}
		if(ValidationUtil.isNullOrEmpty(response.getExceptionDetailList())){
			response.setExceptionDetailList(new ArrayList<ExceptionDetail>());
		}
		TransformUtil.setErrors(response.getExceptionDetailList(),bussinesErrorEnum,params);

		response.setSuccess(false);
	}

	public static void setErrors(List<ExceptionDetail> exceptionDetList ,BussinesErrorEnum errorEnum,Object[] params){
		ExceptionDetail exceptionDet=new ExceptionDetail();
		exceptionDet.setErrorMessage(MessageFormat.format(ERROR_PROPERTIES.getProperty(errorEnum.name()) , params))  ;
		exceptionDet.setErrorCode(errorEnum.getCode());
		exceptionDetList.add(exceptionDet);
	
	}
	
	

	

	
	public static <T> T  getNewInstance(Class<T> type){
		try {
			return type.newInstance();
		} catch (InstantiationException e) {
			CrmGenericException.throwExcepcion(e);
		} catch (IllegalAccessException e) {
			CrmGenericException.throwExcepcion(e);
		}
		return null;
	}


	/**
	 * Agrega al response list por medio del transformer la lista de VO a
	 * transformar
	 * 
	 * @param transformerClass
	 * @param voList
	 * @param responseList
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T, V, B extends BaseTransformer<T, V>> void transformListTransformer(
			Class<B> transformerClass, List<V> inputList, List<T> outputList) {
		if (ValidationUtil.isNotNullOrEmpty(inputList)){
			BaseTransformer<T, V> transformer;
			try {
				transformer = getNewInstance(transformerClass);
			} catch (Exception e) {
				throw new CrmGenericException(e,
						BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION);
			}
			for (V vo : inputList) {
				if (transformer != null && transformer.getResponse(vo) != null)
					outputList.add(transformer.getResponse(vo));
			}
		}
	}



	/**
	 * Convierte un objeto en una cadena de tipo JSON
	 * 
	 * @param object
	 * @return
	 */
	public static String convertObjectToJsonString(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}

	/**
	 * Convierte una cadena json a objeto
	 * 
	 * @param json
	 * @param clase
	 * @return
	 */
	public static <T> T convertJsontStringToObject(String json,
			Class<T> outClass) {
		Gson gson = new Gson();
		return gson.fromJson(json, outClass);
	}

	/**
	 * Convierte una cadena json a objeto
	 * 
	 * @param json
	 * @param clase
	 * @return
	 */
	public static <T> T convertJsontStringToObject(String json, Type type) {
		Gson gson = new Gson();
		return gson.fromJson(json, type);
	}



	/**
	 * Transforma el valor booleano de contactable true(No) false(Si)
	 * 
	 * @param contactable
	 * @return
	 */
	public static String getContactable(Boolean contactable) {
		return contactable ? STRING_NO : STRING_YES;
	}

	/**
	 * Metodo que rompe una coleccion generica en una lista de paginas
	 * 
	 * @param collection
	 * @param pageSize
	 * @return
	 */

	public static <T> List<List<T>> getPages(Collection<T> collection,
			final Integer pageSize) {
		Integer tamanio;
		if (ValidationUtil.isNull(collection)) {
			return Collections.emptyList();
		}
		List<T> list = new ArrayList<T>(collection);
		if (ValidationUtil.isNull(pageSize) || pageSize <= 0
				|| pageSize > list.size()) {
			tamanio = list.size();
		} else {
			tamanio = pageSize;
		}
		int numPages = (int) Math.ceil((double) list.size() / (double) tamanio);
		List<List<T>> pages = new ArrayList<List<T>>(numPages);
		for (int pageNum = 0; pageNum < numPages; pageNum++) {
			pages.add(list.subList(pageNum * tamanio,
					Math.min((pageNum + 1) * tamanio, list.size())));
		}
		return pages;
	}

	/**
	 * Convierte un entero a boleano bajo la regla de que 0 o null es false.
	 * 
	 * @param val
	 * @return
	 */
	public static boolean intToBoolean(Integer val) {
		if (ValidationUtil.isNull(val) || val == 0) {
			return false;
		}
		return true;
	}

	public static <T> void addListValidateNull(final List<T> lista, T object) {
		if (ValidationUtil.isNotNull(object)) {
			lista.add(object);
		}
	}

	public static <T, K> List<T> mapToList(Map<K, T> mapa) {
		List<T> lista = null;
		if (ValidationUtil.isNotNull(mapa)) {
			lista = new ArrayList<T>();
			for (Entry<K, T> ent : mapa.entrySet()) {
				lista.add(ent.getValue());
			}
		}
		return lista;
	}

	/**
	 * Valida que si un entero es nulo regrese 0.
	 * 
	 * @param val
	 * @return
	 */
	public static Long numberNullToZero(final Long val) {
		Long res = val;
		if (val == null) {
			res = 0L;
		}
		return res;
	}

	/**
	 * Convierte un entero a boleano bajo la regla de que 0 o null es false.
	 * 
	 * @param val
	 * @return
	 */
	public static BigDecimal numberNullToZeroBigdecimal(final BigDecimal val) {
		BigDecimal res = val;
		if (val == null) {
			res = BigDecimal.ZERO;
		}
		return res;
	}

	/**
	 * Realiza operaciones de promedios
	 * 
	 * @param actualWeight
	 * @param actualProm
	 * @param acumWeight
	 * @param acumProm
	 * @return
	 */
	public static Object[] getRealProm(Long actualWeight,
			BigDecimal actualProm, Long acumWeight, BigDecimal acumProm) {
		BigDecimal promRes;
		Object[] res = new Object[2];
		Long sumTotalMembers;
		promRes = acumProm.multiply(new BigDecimal(acumWeight)).add(
				numberNullToZeroBigdecimal(actualProm).multiply(
						new BigDecimal(numberNullToZero(actualWeight))));
		sumTotalMembers = acumWeight + numberNullToZero(actualWeight);
		if (sumTotalMembers != 0) {
			promRes = promRes.divide(new BigDecimal(sumTotalMembers), 2,
					BigDecimal.ROUND_HALF_UP);
		}
		res[0] = promRes;
		res[1] = sumTotalMembers;
		return res;
	}

	// **

	public static void concatNotNullStringAndSpace(final StringBuilder sb,
			String cadena) {
		if (ValidationUtil.isNotNull(cadena)) {
			sb.append(STRING_SPACE);
			sb.append(cadena);
		}
	}

	public static BigDecimal roudingTo3(BigDecimal val) {
		return val.setScale(3, BigDecimal.ROUND_UP);
	}

	public static BigDecimal validateIsNull(BigDecimal val) {
		BigDecimal num = BigDecimal.ZERO;
		if (ValidationUtil.isNotNull(val)) {
			num = val;
		}
		return num;
	}

	/**
		 * 
		 */
	public static Integer getInteger(BigDecimal big) {
		Integer result = null;
		if (big != null) {
			result = big.intValueExact();
		}
		return result;
	}

	public static boolean isZero(BigDecimal val1) {
		boolean res = false;
		if (val1.compareTo(BigDecimal.ZERO) == 0) {
			res = true;
		}
		return res;
	}

	public static boolean isMenorZero(BigDecimal val1) {
		boolean res = false;
		if (val1.compareTo(BigDecimal.ZERO) < 0) {
			res = true;
		}
		return res;
	}

	public static boolean isMayorZero(BigDecimal val1) {
		boolean res = false;
		if (val1.compareTo(BigDecimal.ZERO) > 0) {
			res = true;
		}
		return res;
	}

	public static boolean isMayor(BigDecimal val1, BigDecimal val2) {
		boolean res = false;
		if (val1.compareTo(val2) > 0) {
			res = true;
		}
		return res;
	}

	public static boolean isMenor(BigDecimal val1, BigDecimal val2) {
		boolean res = false;
		if (val1.compareTo(val2) < 0) {
			res = true;
		}
		return res;
	}
	

	
}
