package com.iebl.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;



import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.common.error.CrmGenericException;


/**
 * 
 * @author vcamacho
 *
 */
public class DateUtils {


	private static final String FORMAT_YEAR_MONTH_DAY_GUION = "yyyy-MM-dd";
	private static final String FORMAT_ESTANDART_STRING_DATE = "dd/MM/yyyy";

	/**
	 * Default constructor
	 */
	private DateUtils() {
	}

	/**
	 * Obtiene la cadena standart para mostrar fechas dd/mm/YYYY
	 * 
	 * @param cal
	 * @return
	 */
	public static String getStandartStringDate(Calendar cal) {
		SimpleDateFormat format = new SimpleDateFormat(
				FORMAT_ESTANDART_STRING_DATE);
		return format.format(cal.getTime());
	}

	/**
	 * Obtiene un calendar de cierto string
	 * 
	 * @param stringDate
	 * @param formatPattern
	 * @return
	 */
	public static Calendar getCalendar(String stringDate, String formatPattern) {
		SimpleDateFormat format = new SimpleDateFormat(formatPattern);
		Date date = null;
		Calendar cal = Calendar.getInstance();
		try {
			date = format.parse(stringDate);
			cal.setTime(date);
		} catch (ParseException e) {
			throw new CrmGenericException(e,BussinesVsmartCommonErrorEnumImp.PARSING_DATE_ERROR);
		}
		return cal;
	}

	/**
	 * Obtiene un calendar del patron yyyy-mm-dd
	 * 
	 * @param stringDate
	 * @return
	 */
	public static Calendar getCalendarYMDGuion(String stringDate) {

		return getCalendar(stringDate, FORMAT_YEAR_MONTH_DAY_GUION);
	}

	/**
	 * Obtiene la diferencia en dias meses y anios de la fecha startDate a la
	 * fecha endDate
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<Integer> getYearMontDaysDiference(Calendar startDate,
			Calendar endDate) {
		List<Integer> result = new ArrayList<Integer>();
		int antYears;
		int antMonts;
		int antDays;
		antYears = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
		antMonts = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH);
		antDays = endDate.get(Calendar.DAY_OF_MONTH)
				- startDate.get(Calendar.DAY_OF_MONTH);
		if (antDays < 0) {
			antMonts--;
			antDays = antDays + 30;
		}
		if (antMonts < 0) {
			antYears = antYears - 1;
			antMonts = 12 + antMonts;
		}
		result.add(antYears);
		result.add(antMonts);
		result.add(antDays);

		return result;
	}

	/**
	 * Regresa una fecha atantos anios se especifique
	 * 
	 * @param date
	 * @param years
	 * @return
	 */
	public static Date getDateBackYear(Date date, int years) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - years);
		return cal.getTime();
	}

	/**
	 * Obtiene la fecha en string con formato yyyyMMdd
	 * 
	 * @param date
	 * @return
	 */
	public static String getStringDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat(
				FORMAT_YEAR_MONTH_DAY_GUION);
		return format.format(date);
	}

	/**
	 * Valida si la fecha sin horas es la fecha actual
	 * 
	 * @param date
	 * @return
	 */
	public static Boolean validateCurrentDay(Date date) {
		Calendar todayCalendar = Calendar.getInstance();

		// set the calendar to start of today
		todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
		todayCalendar.set(Calendar.MINUTE, 0);
		todayCalendar.set(Calendar.SECOND, 0);
		todayCalendar.set(Calendar.MILLISECOND, 0);

		// and get that as a Date
		Date today = todayCalendar.getTime();

		Calendar calendarSpecified = Calendar.getInstance();
		calendarSpecified.setTime(date);
		// set the calendar to start of today
		calendarSpecified.set(Calendar.HOUR_OF_DAY, 0);
		calendarSpecified.set(Calendar.MINUTE, 0);
		calendarSpecified.set(Calendar.SECOND, 0);
		calendarSpecified.set(Calendar.MILLISECOND, 0);

		// and get that as a Date
		Date dateSpecified = calendarSpecified.getTime();

		return today.equals(dateSpecified);
	}

	/**
	 * Devuelve un objeto date en string en el formato dd/MM/yyyy
	 * 
	 * @param specifiedDate
	 * @return
	 */
	public static String getStringStandardDate(Date specifiedDate) {
		if(ValidationUtil.isNull(specifiedDate)){
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_ESTANDART_STRING_DATE);
		return format.format(specifiedDate);
		
	}
	/**
	 * Convierte de milisegundos  a minutos redondeado hacia arriba
	 * @param milSec
	 * @return
	 */
	public static Long getMinutes(Long milSec){
		if(milSec!=null && milSec!=0){
			return (long) Math.ceil(milSec/60000.0);
		}
		return milSec;
	}
/**
 * Regresa la fecha con horas segundos y minutos en cero tantos dias atras se especifique
 * @param date
 * @param days
 * @return
 */
	public static Date getDateBackDays(Date date,int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		boolean nextYear;
		for(int i=0;i<days;i++ ){

			if(cal.get(Calendar.DAY_OF_YEAR)==1){
				nextYear=true;
			}else{
				nextYear=false;
			}
			if(nextYear){
				cal.roll(Calendar.YEAR, false);
			}
			cal.roll(Calendar.DAY_OF_YEAR, false);
		
		}
		return cal.getTime();
	}
	
	public static Date getDateFordwardDays(Date date,int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		boolean nextYear=false;
		for(int i=0;i<days;i++ ){
			
			if(nextYear){
				cal.roll(Calendar.YEAR, true);
			}
			
			if(cal.get(Calendar.DAY_OF_YEAR)==1){
				nextYear=true;
			}else{
				nextYear=false;
			}
	
			cal.roll(Calendar.DAY_OF_YEAR, true);
		
		}
		return cal.getTime();
	}
	
	 /**
     * Convierte un objeto XMLGregorianCalendar a Date
     * 
     * @param calendar
     * @return
     */
    public static Date toDate( XMLGregorianCalendar calendar ) {
        if( calendar == null ){
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
    
    /**
     * Devuelve un objeto XMLGregorianCalendar a partir de un objeto Date
     * 
     * @param fecha Objeto Date
     * @return
     */
    public static XMLGregorianCalendar dateToXmlGregorianCalendar( Date fecha ) {
        GregorianCalendar calendario = new GregorianCalendar();
        XMLGregorianCalendar xmlCalendario = null;

        calendario.setTime( fecha );

        try{
            xmlCalendario = DatatypeFactory.newInstance().newXMLGregorianCalendar( calendario );
        }
        catch( DatatypeConfigurationException ex ){
        	throw new CrmGenericException(ex,BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION);
        }

        return xmlCalendario;
    }
    
    /**
	 * Devuelve un objeto Date en formato dd/MM/yyyy
	 * de un String
	 * @param specifiedDate
	 * @return
	 */
    public static Date convertFecha (String fecha) {
    	
    	XMLGregorianCalendar xmlCalendario = null;
    	Date date = null;
		try {
			xmlCalendario = DatatypeFactory.newInstance().newXMLGregorianCalendar(fecha);
			date = xmlCalendario.toGregorianCalendar().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_ESTANDART_STRING_DATE);
			formatter.format(date);
           
       }  catch( DatatypeConfigurationException ex ){
       		throw new CrmGenericException(ex,BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION);
       } 
		return date;
	}
    
    /**
	 * Devuelve un objeto Date en formato dd/MM/yyyy
	 * de un String
	 * @param specifiedDate
	 * @return
	 */
    public static Date convertFecha2 (String fecha) {

    	Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_ESTANDART_STRING_DATE);
			date = formatter.parse(fecha);
       }  catch(ParseException e) {
   		throw new CrmGenericException(e,BussinesVsmartCommonErrorEnumImp.PARSING_DATE_ERROR);
   	}
		return date;
	}
    /**
     * Entrega un date con el mes y el año del dia 1 de esa fecha
     * @param year
     * @param month
     * @return
     */
	public static Date getDateFirstDay(Long year,Long month){
		if(ValidationUtil.isNotNull(year)&&ValidationUtil.isNotNull(month)){
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.YEAR, year.intValue());
			cal.set(Calendar.MONTH,month.intValue()-1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.AM_PM,Calendar.AM);
			return cal.getTime();
		}
		return null;
	}
    
}
