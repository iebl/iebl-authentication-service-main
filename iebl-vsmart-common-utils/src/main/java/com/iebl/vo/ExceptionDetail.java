package com.iebl.vo;

import java.io.Serializable;

public class ExceptionDetail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1322383971953179312L;
	private String errorCode;
	private String errorMessage;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
