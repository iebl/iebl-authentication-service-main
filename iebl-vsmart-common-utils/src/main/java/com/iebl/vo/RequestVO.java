package com.iebl.vo;

import java.io.Serializable;


public class RequestVO<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -457596151862027145L;
	private HeaderVO header;
	private PaginationVO paginationVO;
	private T reqData;
	public HeaderVO getHeader() {
		return header;
	}
	public void setHeader(HeaderVO header) {
		this.header = header;
	}
	public PaginationVO getPaginationVO() {
		return paginationVO;
	}
	public void setPaginationVO(PaginationVO paginationVO) {
		this.paginationVO = paginationVO;
	}
	public T getReqData() {
		return reqData;
	}
	public void setReqData(T reqData) {
		this.reqData = reqData;
	}

	
}
