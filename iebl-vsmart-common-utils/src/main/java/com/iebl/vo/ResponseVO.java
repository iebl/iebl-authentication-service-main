package com.iebl.vo;

import java.io.Serializable;
import java.util.List;

public class ResponseVO<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8150093201183071077L;
	private PaginationVO paginationVO;
	private boolean success;
	private T respData;
	private List<ExceptionDetail> exceptionDetailList;
	public PaginationVO getPaginationVO() {
		return paginationVO;
	}
	public void setPaginationVO(PaginationVO paginationVO) {
		this.paginationVO = paginationVO;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public T getRespData() {
		return respData;
	}
	public void setRespData(T respData) {
		this.respData = respData;
	}
	public List<ExceptionDetail> getExceptionDetailList() {
		return exceptionDetailList;
	}
	public void setExceptionDetailList(List<ExceptionDetail> exceptionDetailList) {
		this.exceptionDetailList = exceptionDetailList;
	}
	
	
}
