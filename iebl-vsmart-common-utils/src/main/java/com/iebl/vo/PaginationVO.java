package com.iebl.vo;

import java.io.Serializable;


public class PaginationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7590903499685130613L;
	private Integer size;
	private String order;
	private Integer index;
	
	private Long totalSize;
	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Long getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(Long totalSize) {
		this.totalSize = totalSize;
	}
	public String getOrderType() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	
}
