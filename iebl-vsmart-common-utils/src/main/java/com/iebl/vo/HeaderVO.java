package com.iebl.vo;

import java.io.Serializable;
import java.util.Date;

public class HeaderVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6358141664626299543L;
	private String user; 

	private String ip;
	private String device;//android,ios,webpage
	private String module;
	private String lenguage;//MX
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getLenguage() {
		return lenguage;
	}
	public void setLenguage(String lenguage) {
		this.lenguage = lenguage;
	}

	

}
