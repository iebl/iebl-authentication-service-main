package com.iebl.common.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mockit.integration.junit4.JMockit;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.iebl.base.BaseTransformer;
import com.iebl.common.error.BussinesVsmartCommonErrorEnumImp;
import com.iebl.vo.ExceptionDetail;
import com.iebl.vo.PaginationVO;
import com.iebl.vo.ResponseVO;




@RunWith(JMockit.class)
public class TransformUtilTest {


	@Test
	public void getBigInteger() {
		TransformUtil.getBigInteger(1);
	}

	@Test
	public void getXMLCalendar() {
		
		TransformUtil.getXMLCalendar(Calendar.getInstance());

	}

	@Test
	public void getDate() {
		TransformUtil.getDate(TransformUtil.getXMLCalendar(new Date()));
	}

	@Test
	public void searchException() {
		TransformUtil.searchException(Exception.class, new Exception());
	}
	@Test
	public void transformZeusResponseFromData(){
		ResponseVO<String> zresponse=new ResponseVO<String>();
		zresponse.setPaginationVO(new PaginationVO());
		zresponse.setRespData("data");
		TransformUtil.transformResponseFromData(zresponse, 1l);
		
	}
	
	@Test
	public  void  transformZeusOnException() {
		ResponseVO<String> zresponse2=new ResponseVO<String>();
		try{
		TransformUtil.transformOnException(zresponse2, new Exception());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Test
	public  void setErrors(){
		List<ExceptionDetail> zheader=new ArrayList<ExceptionDetail>();
		TransformUtil.setErrors(zheader, BussinesVsmartCommonErrorEnumImp.DATA_NOT_EXIST,null);
		TransformUtil.setErrors(zheader, BussinesVsmartCommonErrorEnumImp.UNEXPECTED_EXCEPTION,null);
	}
	@Test
	public void  getNewInstance(){
		TransformUtil.getNewInstance(String.class);

	}

	
	class TransformTest implements BaseTransformer<Object,Object>{

		@Override
		public String getResponse(Object in) {
			
			return "ok";
		}
		
	}
	@Test
	public void transformListTransformer(){
		List<Object> inputList=new ArrayList<Object>();
		List<Object> outputList=new ArrayList<Object>();
		inputList.add("1");
		
		TransformUtil.transformListTransformer(TransTest.class, inputList, outputList);
	}
	@Test
	public void convertObjectToJsonString() {
		TransformUtil.convertObjectToJsonString("String");
	}
	@Test
	public void convertJsontStringToObject() {
		TransformUtil.convertJsontStringToObject("{}", Object.class);
	}

	@Test
	public void getContactable() {
		TransformUtil.getContactable(true);
		TransformUtil.getContactable(false);
	}
	@Test
	public void getPages() {
		TransformUtil.getPages(new ArrayList<String>(), 1);
	}
	@Test
	public void intToBoolean() {
		TransformUtil.intToBoolean(1);
		TransformUtil.intToBoolean(0);
	}
	@Test
	public  void addListValidateNull() {
		List<String> lista=new ArrayList<String>();
		
		TransformUtil.addListValidateNull(lista, "");
	}
	@Test
	public void mapToList() {
		
		Map<String,String> mapa =new TreeMap<String, String>();
		mapa.put("x", "x");
		TransformUtil.mapToList(mapa );
	}
	@Test
	public void numberNullToZero() {
		TransformUtil.numberNullToZero(1L);
		TransformUtil.numberNullToZero(null);
	}
	@Test
	public void numberNullToZeroBigdecimal() {
		TransformUtil.numberNullToZeroBigdecimal(new BigDecimal("1"));
		TransformUtil.numberNullToZeroBigdecimal(null);
	}

	@Test
	public void getRealProm() {
		TransformUtil.getRealProm(1l, new BigDecimal("1"),1l, new BigDecimal("1"));
	}

	@Test
	public  void concatNotNullStringAndSpace() {
		TransformUtil.concatNotNullStringAndSpace(new StringBuilder(), "ppp ");
	}
	@Test
	public void roudingTo3() {
		TransformUtil.roudingTo3(new BigDecimal("12.3445"));
	}
	@Test
	public void validateIsNull() {
		TransformUtil.validateIsNull(new BigDecimal("1"));
		TransformUtil.validateIsNull(null);
	}

	@Test
	public void getInteger() {
		TransformUtil.getInteger(null);
	}
	@Test
	public void isZero() {
		TransformUtil.isZero(new BigDecimal("0"));
		TransformUtil.isZero(new BigDecimal("1"));
	}
	@Test
	public void isMenorZero() {
		TransformUtil.isMenorZero(new BigDecimal("1"));
		TransformUtil.isMenorZero(new BigDecimal("-1"));
	}
	@Test
	public void isMayorZero() {
		TransformUtil.isMayorZero(new BigDecimal("1"));
		TransformUtil.isMayorZero(new BigDecimal("-1"));
	}
	@Test
	public void isMayor() {
		TransformUtil.isMayor(new BigDecimal("1"), new BigDecimal("2"));
		TransformUtil.isMayor(new BigDecimal("2"), new BigDecimal("1"));
	}
	@Test
	public void isMenor() {
		TransformUtil.isMenor(new BigDecimal("1"), new BigDecimal("2"));
		TransformUtil.isMenor(new BigDecimal("2"), new BigDecimal("1"));
	}
	

	
}
